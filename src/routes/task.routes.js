import { Router } from "express";
const router = Router();

//db con
import { connect } from "../database";
import { ObjectID } from "mongodb";


router.get('/', async (req, res) => {
    const db = await connect();
    const result = await db.collection('tasks').find().toArray();

    res.json(result);
});
router.post('/', async (req, res) => {
    const db = await connect();
    const task = {
        title: req.body.title,
        description: req.body.description
    };
    console.log(task);
    const result = await db.collection('tasks').insert(task);
    res.json(result.ops[0]);


})
router.get('/:id', async (req, res) => {
    const { id } = req.params;
    console.log(id);
    const db = await connect();
    const result = await db.collection('tasks').findOne({ _id: ObjectID(id) });
    res.json(result);

});
router.delete('/:id', async (req, res) => {
    const { id } = req.params;
    const db = await connect();
    await db.collection('tasks').deleteOne({_id: ObjectID(id)});
    res.json({
        message: `Task ${id} removed`,
        result
    });

});
router.put('/:id', async (req, res) => {
    const { id } = req.params;
    const updateTask = {
        title: req.body.title,
        description: req.body.description
    }
    console.log(updateTask);
    const db = await connect();
    const result = await db.collection('task').updateOne({ _id: ObjectID(id) }, {$set: updateTask });
    res.json({
        message: `Task ${id} edited`,
        result
    });

});
export default router;