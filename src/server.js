import express,{json} from "express";
const app = express();

//routes
import IndexRoutes from "./routes/index.routes";
import TaskRoutes from "./routes/task.routes";
//settings
app.set('port', process.env.PORT || 3000)

//middlewares
app.use(json())

//routes
app.use(IndexRoutes);
app.use('/tasks',TaskRoutes);

export default app;